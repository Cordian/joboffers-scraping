# JobOffers Scraping
JobOffers Scraping is my first project I've created to learn Python basics.  

## Table of contents
* [General info](#info)
* [Technologies](#technologies)
* [Setup](#setup)
* [Features](#features)

## General info
JobOffers Scraping is web application, where we can check the latest job offers based on our preferences and history of viewed offers.

## Technologies
* Python 3.8.6
* Django 2.2.5
* SQLite 3.33.0
* Bootstrap 5.0
* HTML

## Setup
To run this project, install **Python**, then install **Django** using terminal:
```
python -m pip install Django
```
Next, clone the project:
```
git clone https://gitlab.com/Cordian/joboffers-scraping.git
```
Then, open terminal in **_.../mysite_** project directory and run Django server:
```
python manage.py runserver
```
By default, server starts at:
```
http://localhost:8000/jobs/
```

## Features
* **New offers notification**  
By default, job offers button "Oferty" is grey, when new offers are available button becames blue.  
  <kbd>
  <img src="https://i.postimg.cc/MpstZn6f/home.png" width="554"/>  
  </kbd>

* **Job offers**  
We can see the latest offers by clicking button "Oferty".  
  <kbd>
  <img src="https://i.postimg.cc/d0L6hJ3H/offers.png" width="554"/>
  </kbd>
* **Preferences**  
There is a few options to configure job offers preferences after clicking button "Preferencje". Preferences are saved in the database.  
  <kbd>
  <img src="https://i.postimg.cc/0NFfmhJ8/preferences-1.png" width="554"/>
  </kbd>
* **History**  
We can see history of viewed offers by clicking button "Historia". History is saved in the database.  
  <kbd>
  <img src="https://i.postimg.cc/ht4pqLBq/history.png" width="554"/>
  </kbd>
