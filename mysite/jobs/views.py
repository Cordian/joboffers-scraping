from django.shortcuts import render, redirect
from .methods import save_preferences_in_db, fetch_offers, open_offer_url
from .models import *


def home(request):
    if Offer.objects.filter(opened=0):
        notification = 1
    else:
        notification = 0
    print(notification)
    context = {'notification': notification}
    return render(request, 'jobs/home.html', context)


def inbox(request):
    new_offers = Offer.objects.filter(opened=0)
    context = {'new_offers': new_offers}
    return render(request, 'jobs/inbox.html', context)


def preferences(request):
    localizations = Localization.objects.order_by('id')
    roles = Role.objects.order_by('id')
    technologies = Technology.objects.order_by('id')
    experience = Experience.objects.order_by('id')
    context = {
        'localizations': localizations,
        'roles': roles,
        'technologies': technologies,
        'experience': experience
    }
    return render(request, 'jobs/preferences.html', context)


def save_preferences(request):
    if request.method == 'POST':
        save_preferences_in_db(request)

    return redirect('jobs:fetch')


def history(request):
    seen_before_offers = Offer.objects.filter(opened=1)
    context = {'seen_before_offers': seen_before_offers}
    return render(request, 'jobs/history.html', context)


def fetch(request):
    fetch_offers()
    return redirect('jobs:home')


def open_offer(request):
    if request.method == 'POST':
        offer_url = open_offer_url(request)
    else:
        offer_url = 'jobs:inbox'

    return redirect(offer_url)
