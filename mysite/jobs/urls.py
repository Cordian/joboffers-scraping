from django.urls import path

from . import views

app_name = 'jobs'
urlpatterns = [
    path('', views.fetch, name='fetch'),
    path('home/', views.home, name='home'),
    path('inbox/', views.inbox, name='inbox'),
    path('preferences/', views.preferences, name='preferences'),
    path('save_preferences/', views.save_preferences, name='save_preferences'),
    path('history/', views.history, name='history'),
    path('open_offer/', views.open_offer, name='open_offer'),
]
