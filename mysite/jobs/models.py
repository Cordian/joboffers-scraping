from django.db import models


class Offer(models.Model):
    offer_title = models.CharField(max_length=200)
    offer_url = models.CharField(max_length=300)
    opened = models.BooleanField(default=False)


class Localization(models.Model):
    value = models.CharField(max_length=200)
    checked = models.BooleanField(default=False)


class Role(models.Model):
    value = models.CharField(max_length=200)
    name = models.CharField(max_length=200, default='')
    checked = models.BooleanField(default=False)


class Technology(models.Model):
    value = models.CharField(max_length=200)
    name = models.CharField(max_length=200, default='')
    checked = models.BooleanField(default=False)


class Experience(models.Model):
    value = models.CharField(max_length=200)
    name = models.CharField(max_length=200, default='')
    checked = models.BooleanField(default=False)
