import requests
from bs4 import BeautifulSoup
from .models import *
from urllib import request as rqst


def scrap_job_offers(url: str, offers_list: [], current_page_number: int = 1):
    r = requests.get(url)
    soup = BeautifulSoup(r.content, features='lxml')
    data = soup.find_all("a", {"class": "search-list-item"})

    for item in data:
        title = str.strip(item.contents[2].contents[1].contents[1].text)
        offers_list.append(Offer(offer_title=title, offer_url=item.get('href')))

    next_page = get_next_page(url)
    if next_page is not None:
        scrap_job_offers(next_page, offers_list, current_page_number + 1)


def get_next_page(url: str):
    r = requests.get(url)
    soup = BeautifulSoup(r.content, features='lxml')
    url = None

    data = soup.find_all("a", {"class": "next"})
    for link in data:
        if 'http' in link.get('href'):
            url = link.get('href')

    return url


def save_preferences_in_db(request):
    localization_choices_id = request.POST.getlist('localization_choice')
    localizations = Localization.objects.order_by('-id')
    save_preference(localizations, localization_choices_id)

    role_choices_id = request.POST.getlist('role_choice')
    roles = Role.objects.order_by('-id')
    save_preference(roles, role_choices_id)

    technology_choices_id = request.POST.getlist('technology_choice')
    technologies = Technology.objects.order_by('-id')
    save_preference(technologies, technology_choices_id)

    experience_choices_id = request.POST.getlist('exp_choice')
    exp = Experience.objects.order_by('-id')
    save_preference(exp, experience_choices_id)


def save_preference(db_items, db_items_id):
    for item in db_items:
        if str(item.id) in db_items_id:
            item.checked = 1
        else:
            item.checked = 0
        item.save()


def bulldogjob_url_by_preferences():
    url = "https://bulldogjob.pl/companies/jobs/"

    localizations = Localization.objects.filter(checked=1)
    roles = Role.objects.filter(checked=1)
    technologies = Technology.objects.filter(checked=1)
    experience = Experience.objects.filter(checked=1)

    if localizations:
        loc_names = '/city,' + ','.join(l.value for l in localizations)
    else:
        loc_names = ''

    if roles:
        role_names = '/role,' + ','.join(r.value for r in roles)
    else:
        role_names = ''

    if technologies:
        tech_names = '/skills,' + ','.join(t.value for t in technologies)
    else:
        tech_names = ''

    if technologies:
        exp_names = '/experience_level,' + ','.join(e.value for e in experience)
    else:
        exp_names = ''

    if loc_names or role_names or tech_names or exp_names:
        url += 's' + loc_names + role_names + tech_names + exp_names

    return url


def fetch_offers():
    found_offers = []
    url = bulldogjob_url_by_preferences()
    scrap_job_offers(url=url, offers_list=found_offers)

    db_offers = Offer.objects.order_by('id').all()
    found_offer_urls = [offer.offer_url for offer in found_offers]
    for offer in db_offers:
        if offer.offer_url not in found_offer_urls:
            offer.delete()

    db_offers = Offer.objects.order_by('id').all()
    db_offer_urls = [offer.offer_url for offer in db_offers]
    for offer in found_offers:
        if offer.offer_url not in db_offer_urls:
            offer.opened = 0
            offer.save()


def open_offer_url(request):
    offer_url = request.POST.get('url')
    if offer_url is not None:
        offer = Offer.objects.filter(offer_url=offer_url)[0]
        offer.opened = 1
        offer.save()
    return offer_url
